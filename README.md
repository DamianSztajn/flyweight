Py�ek (ang. Flyweight) � strukturalny wzorzec projektowy, kt�rego celem jest zmniejszenie wykorzystania pami�ci poprzez popraw� efektywno�ci obs�ugi du�ych obiekt�w zbudowanych z wielu mniejszych element�w poprzez wsp�dzielenie wsp�lnych ma�ych element�w. Nale�y do grupy wzorc�w skatalogowanych przez Gang of Four.


Przyk�adowe zastosowanie

Wzorzec Py�ek stosuje si� tam, gdzie jedna klasa ma wiele egzemplarzy, a ka�dy z tych egzemplarzy mo�e by� sterowany w ten sam spos�b. Przyk�adowo ten�e wzorzec mo�na zastosowa� w programie wspomagaj�cym modelowanie przestrzenne terenu. Jednym z wielu element�w, kt�re musz� si� w nim znale�� s� obiekt reprezentuj�cy drzewo. Zak�adamy, �e obiekt taki posiada informacje o wygl�dzie drzewa oraz jakie� inne jego cechy, przy czym tak�e jego wysoko�ci oraz jego wsp�rz�dne po�o�enia. Podczas modelowania wielkich kompleks�w zieleni z�o�onych z wielu egzemplarzy drzewa program mo�e zacz�� dzia�a� niezadowalaj�co wolno. Aby upora� si� z takim problemem mo�na zastosowa� wzorzec Py�ek. Po zastosowaniu tego wzorca projektowego zamiast tworzy� indywidualny egzemplarz klasy (obiekt) dla ka�dego drzewa mo�liwe jest stworzenie kompleksowego obiektu, kt�ry b�dzie w sobie zawiera� informacje o wszystkich drzewach renderowanych na modelowanym terenie. W takim obiekcie z racji tego, �e wszystkie renderowane drzewa maj� pewne cechy wsp�lne, takie jak np. wygl�d, informacje te b�d� zapisane tylko raz (cho� drzew mo�e by� tysi�ce), a zwielokrotniane b�d� jedynie informacje r�ne dla ka�dego drzewa tak jak np. wsp�rz�dne i wysoko��. Spos�b wykonania tej idei oraz jej implementacja zale�y od woli programisty.
Struktura wzorca

Istot� wzorca jest podzia� danych przechowywanych w ci�kim obiekcie na wewn�trzne i zewn�trzne. Do klasy danych wewn�trznych wybierane s� te sk�adowe ci�kiej klasy pierwotnej, kt�rych warto�ci cz�sto si� powtarzaj�. Pozosta�e sk�adowe stanowi� dane zewn�trzne. Po ustaleniu podzia�u zamiast ci�kich obiekt�w wzorzec wprowadza odpowiadaj�ce im obiekty klient�w oraz tzw. obiekty py�k�w. Obiekty py�k�w s� tworzone z wybranych wcze�niej danych wewn�trznych. Ka�dy z nich jest wsp�dzielony przez wielu klient�w i nie mo�na go modyfikowa�. Dane zewn�trzne, unikatowe dla ka�dego obiektu klienta, s� dostarczane obiektowi py�ku poprzez okre�lone metody. Wzorzec zawiera dw�ch uczestnik�w - Fabryk� Py�k�w i Py�ek. Klient nie tworzy egzemplarzy typu Py�ek samodzielnie, a jedynie wysy�a do Fabryki ��danie ich udost�pnienia. Fabryka zwraca Klientowi istniej�cy Py�ek lub tworzy nowy, je�li �aden egzemplarz tej klasy jeszcze nie istnieje.
Innymi s�owy

Ide� wzorca projektowego py�ek jest stworzenie prostego mechanizmu wsp�dzielenia obiektu o niewielkim rozmiarze przez wiele obiekt�w w celu zwi�kszenia wydajno�ci systemu pod wzgl�dem zu�ycia pami�ci. Zamiast zapisywa� oddzielnie kilka olbrzymich obiekt�w, dzielmy owe obiekty na mniejsze sk�adowe i elementy kt�re si� w nich powtarzaj� zapisujemy tylko raz,a nie kilka razy. M�wi�c inaczej py�ek ma na celu udost�pnianie pojedynczego ma�ego obiektu wielu klientom(du�ym obiektom). Implementuj�c ten wzorzec projektowy nale�y rozwi�za� problem wsp�lnego dost�pu do wsp�dzielonych danych. Jednym ze sposob�w realizacji tego mechanizmu jest podzia� danych przechowywanych w atrybutach obiekt�w (te mniejsze elementy) na wsp�dzielone dane wewn�trzne intrinsicState oraz niewsp�dzielone, unikatowe dla ka�dego obiektu dane zewn�trzne allState. Czyli dwie grupy z�o�one z ma�ych element�w.

Wzorzec ten mo�na formalnie zrealizowa� za pomoc� trzech g��wnych element�w:

    elementu abstrakcyjnego Flyweight definiuj�cego operacje s�u��ce do przyjmowania i odtwarzania stanu zewn�trznego obiektu opisywanego przez klas� UnsharedConcreteFlyweight
    obiektu tworzonego na bazie klasy ConcreteFlyweight przechowuj�cego stan wewn�trzny (wsp�dzielony) obiektu, kt�ry dodatkowo jest niezale�ny od kontekstu wywo�ania
    fabryki FlyweightFactory, kt�rej zadaniem jest kreowanie i sk�adowanie obiekt�w realizuj�cych interfejs Flyweight

Konsekwencje stosowania

Korzy�ci wynikaj�ce z zastosowania tego wzorca to:

    ograniczenie liczby obiekt�w u�ywanych w trakcie wykonywania programu, a co za tym idzie zaoszcz�dzenie pami�ci aplikacji � tym wi�ksze, im wi�cej obiekt�w jest wsp�dzielonych
    sk�adowanie danych stanu wsp�dzielonych obiekt�w odbywa si� w jednej lokalizacji.

Wady wzorca Py�ek to:

    zmniejszenie wydajno�ci aplikacji
    utrata przez pojedyncze, logiczne egzemplarze klasy mo�liwo�ci posiadania zachowa� niezale�nych od pozosta�ych egzemplarzy.